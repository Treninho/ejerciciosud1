<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="com.informaticapinguela.jsp.*" import="java.util.*"
    errorPage="erro.jsp" %>
    <%response.setContentType("text/html");%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Procesador de los datos </title>
<style>
p {font-family: arial; font-size: 22px; }
</style>
</head>
<body>
<h1><%@ include file="header.jsp"%></h1>
<%
// Para probar un error en la p�gina
 // int number= Integer.parseInt("hola"); Para probar de que la p�gina de erro.jsp funciona. 
%>

	<h1>Datos procesados: </h1>
	<p>IP del cliente : <%= request.getRemoteAddr()%></p>
	<p>Nombre: <%=request.getParameter("nome")%></p>
	<p>Frase introducida: <%=request.getParameter("frase")%></p>
	<p>Frase le�da del reves: <%=Procesador2.getReverse(request.getParameter("frase"))%></p>
	<p>Palabra o palabra m�s larga:  <%=Procesador2.getLongest(request.getParameter("frase")) %> </p>
	<p>Palabra o palabra m�s corta: <%=Procesador2.getShortest(request.getParameter("frase"))%></p>
	
	
	<% // con la siguiente operacion si la palabra contiene 5 letras redirecciona a premio.jsp %>
	<%
	String [] cincoDigitos = Procesador2.getTockens(request.getParameter("frase"));
		for (int i=0; i<cincoDigitos.length; i++){
			if (cincoDigitos[i].length() == 5){
				response.sendRedirect("premio.jsp");
			}
		}
	%>
	<br>
<a href="principal.jsp"><%@ include file="footer.jsp"%></a>
</body>
</html>