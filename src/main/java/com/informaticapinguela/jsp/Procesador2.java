package com.informaticapinguela.jsp;

public class Procesador2 {

	// método que devuelve un array de strings 
	public static String[] getTockens(String frase) {
		// creamos array de string. 
		// Con método split nos crea el array sin espacios.
		String [] palabras = frase.split(" ");
		// recorremos bucle para recorrer el array y sacar sus datos.
		for (int i=0; i<palabras.length; i++) {
			System.out.println(palabras[i]);
		}
		return palabras;
	}
	
	// método que devuelve la palabra más larga del array
	// Devuelve la palabra más larga y si hay alguna otra con la misma cantidad la saca también.
	public static String getLongest(String frase) {
		
		// creamos una nueva variabe que toma el valor de la frase con un array 
		String [] comprobarLong = frase.split(" ");
		String longitudMaxima = "";
		String auxiliar = "";
		String longitudIgual = "";
		// recorremos array y le damos el primer valor del array a una variable para poder comparar
		for (int i=0; i<comprobarLong.length; i++) {
			longitudMaxima = comprobarLong[0];
		}
		
		// Bucle donde recorremos el array
		// Con if comparamos con la primera variable (array[0]) con las otras palabras del array
		// Con el .length calculamos el número de letras que tiene cada palabra para ir comprando
		for (int i=0; i<comprobarLong.length; i++) {
			if (longitudMaxima.length() > comprobarLong[i].length()) {
				auxiliar = comprobarLong[i];
			}else if (longitudMaxima.length() < comprobarLong[i].length()){
				longitudMaxima = comprobarLong[i];
			}
			if (longitudMaxima.equals(comprobarLong[i]) == false && longitudMaxima.length() == 
					comprobarLong[i].length()) {
					longitudIgual+=" "+ comprobarLong[i];
				}
				
		}
		if (longitudMaxima.equals(longitudIgual)== true) {
			return longitudIgual;
		}
			return longitudMaxima;

		
	}
	public static String getShortest(String frase) {
		
				
				String [] comprobarLong = frase.split(" ");
				String longitudCorta = "";
				String auxiliar = "";
				String longitudIgual = "";
				
				for (int i=0; i<comprobarLong.length; i++) {
					longitudCorta = comprobarLong[0];
				}
				
				for (int i=0; i<comprobarLong.length; i++) {
					if (longitudCorta.length() < comprobarLong[i].length()) {
						auxiliar = comprobarLong[i];
					}else if (longitudCorta.length() > comprobarLong[i].length()){
						longitudCorta = comprobarLong[i];
					}
					if (longitudCorta.equals(comprobarLong[i]) == false && longitudCorta.length() == 
							comprobarLong[i].length()) {
							longitudIgual+=" "+ comprobarLong[i];
						}
						
				}
				if (longitudCorta.equals(longitudIgual)== true) {
					return longitudIgual;
				}
					return longitudCorta;
				
	}
	// método que devuelve frase al revés
	public static String getReverse(String frase) {
		String cadena= frase;
		StringBuilder builder=new StringBuilder(cadena);
		String CadenaInvertida=builder.reverse().toString();
		return CadenaInvertida;
	}
	
}
